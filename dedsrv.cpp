#include <dlfcn.h>
#include <link.h>
#include <sys/mman.h>
#include <stdio.h>
#include <string.h>
#include <libgen.h>

typedef int(*DedicatedMain_t)(int, char**);
typedef void(*patchfunc_t)(void*);

struct FuncPatch_t {
	const char* s_name;
	patchfunc_t f_patch;
};

void unprot(void* addr, size_t len);
void* dlopenPatch(const char* filename, int flags);

void materialsystemPatch(void* handle) {
	uintptr_t base = ((link_map*)handle)->l_addr;
	uintptr_t* dlopenAddr = (uintptr_t*)(base + 0x94e6c);
	unprot((void*)dlopenAddr, sizeof(uintptr_t));
	*dlopenAddr = (uintptr_t)dlopenPatch - (uintptr_t)dlopenAddr - 4;
	uint16_t* shaderApiCheck = (uint16_t*)(base + 0x25dd6);
	unprot((void*)shaderApiCheck, sizeof(uint16_t));
	*shaderApiCheck = 0x1deb; // jmp +1d
}

void enginePatch(void* handle) {
	uintptr_t base = ((link_map*)handle)->l_addr;
	uint32_t* cgameInit = (uint32_t*)(base + 0x4d37e0);
	unprot((void*)cgameInit, sizeof(uint32_t));
	*cgameInit = 0xc340c031; // xor eax, eax; inc eax; ret
	uint32_t* getMainWindowAddress = (uint32_t*)(base + 0x4d38b0);
	unprot((void*)getMainWindowAddress, sizeof(uint32_t));
	*getMainWindowAddress = 0xc3c031; // xor eax, eax; ret
	uint16_t* initGameDed = (uint16_t*)(base + 0x4aee78);
	unprot((void*)initGameDed, sizeof(uint16_t));
	*initGameDed = 0x78eb; // jmp +78
	uint8_t* steamValidation = (uint8_t*)(base + 0x27bb44);
	unprot((void*)steamValidation, sizeof(uint8_t));
	*steamValidation = 0xeb;
}

void soundemittersystemPatch(void* handle) {
	uintptr_t base = ((link_map*)handle)->l_addr;
	uint32_t* soundInit = (uint32_t*)(base + 0xe4e0);
	unprot((void*)soundInit, sizeof(uint32_t));
	*soundInit = 0xc340c031; // xor eax, eax; inc eax; ret
}

void serverPatch(void* handle) {
	uintptr_t base = ((link_map*)handle)->l_addr;
	uint8_t* preentity = (uint8_t*)(base + 0x440bd0);
	unprot((void*)preentity, sizeof(uint8_t));
	*preentity = 0xc3; // ret
	uint16_t* levelStart = (uint16_t*)(base + 0x8e655f);
	unprot((void*)levelStart, sizeof(uint16_t));
	*levelStart = 0x0deb; // jmp +d
	// player cap edit
	uint8_t* playercap1 = (uint8_t*)(base + 0x5fee35);
	unprot((void*)playercap1, sizeof(uint8_t));
	*playercap1 = 0x1f;
	uint8_t* playercap2 = (uint8_t*)(base + 0x5fee3f);
	unprot((void*)playercap2, sizeof(uint8_t));
	*playercap2 = 0x1f;
	uint8_t* playercap3 = (uint8_t*)(base + 0x5fee66);
	unprot((void*)playercap3, sizeof(uint8_t));
	*playercap3 = 0x20; // +0x20 = +32
}

FuncPatch_t g_FuncPatches[] {
	{ "materialsystem.so", materialsystemPatch },
	{ "engine.so", enginePatch },
	{ "soundemittersystem.so", soundemittersystemPatch },
	{ "server.so", serverPatch },
	{ "", nullptr }
};

void unprot(void* addr, size_t len) {
	uintptr_t startPage = (uintptr_t)addr & 0xFFFFF000;
	uintptr_t endPage = ((uintptr_t)addr + len) & 0xFFFFF000;
	uintptr_t pageLen = endPage - startPage + 0x1000;
	mprotect((void*)startPage, pageLen, PROT_READ | PROT_WRITE | PROT_EXEC);
}

void* dlopenPatch(const char* filename, int flags) {
	const char* fn = basename((char*)filename);

	void* ret = dlopen(fn, flags);

	// printf("-- DLOPEN %s : %s --\n", fn, ret ? "SUCC" : "FAIL");

	if(ret) {
		FuncPatch_t* funcPatches = g_FuncPatches;
		while(*funcPatches->s_name) {
			if(!strcmp(fn, funcPatches->s_name)) {
				printf("Patching code in '%s'\n", funcPatches->s_name);
				funcPatches->f_patch(ret);
			}
			funcPatches++;
		}
	}

	return ret;
}

struct AppSystemInfo_t {
	const char *moduleName;
	const char *interfaceName;
};

typedef bool(*AddSystems_t)(void*, AppSystemInfo_t*);
AddSystems_t AddSystemsReal = nullptr;

bool AddSystemsPatch(void* thisptr, AppSystemInfo_t* systems) {
	// printf("-- ADDSYSTEMS CALLED --\n");
	if(!AddSystemsReal(thisptr, systems)) return false;
	AppSystemInfo_t additionalSystems[] = {
		{ "vscript.so", "VScriptManager009" },
		{ "", "" }
	};
	if(!AddSystemsReal(thisptr, additionalSystems)) return false;
	return true;
}

void dedicatedPatch(void* handle) {
	uintptr_t base = (uintptr_t)((link_map*)handle)->l_addr;
	uintptr_t* dlopenAddr = (uintptr_t*)(base + 0x567ac);
	unprot((void*)dlopenAddr, sizeof(uintptr_t));
	*dlopenAddr = (uintptr_t)dlopenPatch - (uintptr_t)dlopenAddr - 4;
	uintptr_t* AddSystemsAddr = (uintptr_t*)(base + 0x51507);
	unprot((void*)AddSystemsAddr, sizeof(uintptr_t));
	AddSystemsReal = (AddSystems_t)(*AddSystemsAddr + (uintptr_t)AddSystemsAddr + 4);
	*AddSystemsAddr = (uintptr_t)AddSystemsPatch - (uintptr_t)AddSystemsAddr - 4;
}

int main(int argc, char** argv) {
	void* dedicated = dlopen("dedicated.so", RTLD_NOW);
	dedicatedPatch(dedicated);

	DedicatedMain_t DedicatedMain = (DedicatedMain_t)dlsym(dedicated, "DedicatedMain");
	int r = DedicatedMain(argc, argv);

	dlclose(dedicated);
	return r;
}
