CXX := g++
CXXFLAGS := -ggdb -Wall -Werror -O2 -m32
LDFLAGS := -m32
TARGET := dedsrv

SRCS := dedsrv.cpp
OBJS := $(patsubst %.cpp,%.o,$(SRCS))

all: clean $(TARGET)

$(TARGET): $(OBJS)
	$(CXX) $(LDFLAGS) -o $@ $^

%.o: %.cpp
	$(CXX) $(CXXFLAGS) -c $<

clean:
	rm -rf $(TARGET) *.o
