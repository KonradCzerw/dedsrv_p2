# Portal 2 Dedicated Server

This repo contains the required script to run portal 2 dedicated server on linux

USAGE:
- compile with `make`
- put `dedsrv` binary & run.sh in your full Portal 2 install (... steamapps/Portal 2/)
- copy `portal2/bin/linux32/matchmaking.so` to `portal2/bin/linux32/matchmaking_ds.so`
- `./run.sh`
- enjoy :sunglasses:

TODO:
- fix sound
